const verifySignUp = require("./verifySignUp");
const authJwt = require("./verifyJwtToken");
const authController = require("../controllers/authController.js");
const userController = require("../controllers/userController.js");
const bookController = require("../controllers/bookController.js");
const orderController = require("../controllers/orderController.js");

module.exports = function(app) {
  // Auth
  app.post(
    "/api/auth/signup",
    [
      verifySignUp.checkDuplicateUserNameOrEmail,
      verifySignUp.checkRolesExisted
    ],
    authController.signup
  );
  app.post("/api/auth/signin", authController.signin);

    app.get("/api/books/:id",[authJwt.verifyToken], bookController.BooksID);
    app.put("/api/books/:id",[authJwt.verifyToken], bookController.Update);
    app.post("/api/books", [authJwt.verifyToken], bookController.create);
    app.get("/api/books", [authJwt.verifyToken], bookController.Book);
    app.delete("/api/books/:id", [authJwt.verifyToken], bookController.Delete);

    // ORDER
    app.post("/api/order",[authJwt.verifyToken], orderController.create);
    app.get("/api/order", [authJwt.verifyToken], orderController.Orders);
    app.get("/api/order/:id", [authJwt.verifyToken], orderController.OrdersID);
    
  // get all user
  app.get("/api/users", [authJwt.verifyToken], userController.users);

  // get 1 user according to roles
  app.get("/api/test/user", [authJwt.verifyToken], userController.userContent);
  app.get(
    "/api/test/pm",
    [authJwt.verifyToken, authJwt.isPmOrAdmin],
    userController.managementBoard
  );
 app.get(
        "/api/test/admin",
        [authJwt.verifyToken, authJwt.isAdmin],
        userController.adminBoard
      );
    
      // error handler 404
      app.use(function(req, res, next) {
        return res.status(404).send({
          status: 404,
          message: "Not Found"
        });
      });
    
      // error handler 500
      app.use(function(err, req, res, next) {
        return res.status(500).send({
          error: err
        });
      });
    };