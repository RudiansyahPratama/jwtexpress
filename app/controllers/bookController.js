const db = require("../config/db.js");
const Book = db.book;
const asyncMiddleware = require("express-async-handler");


exports.create = asyncMiddleware(async (req, res) => {
    // Save User to Database
        const book = await Book.create({
            title: req.body.title,
            author: req.body.author,
            pages: req.body.pages,
            language: req.body.language
        });

        res.status(201).send({
            status: "Book successfully! Data"
        });
    });

exports.Book = asyncMiddleware(async (req, res) => {
        const book = await Book.findAll({
            attributes: ["id", "title", "author", "pages" ,"language"]
        });
        res.status(201).send({
            status: "Book Show Data",
            book: book
        });
    });

exports.BooksID = asyncMiddleware(async (req, res) => {
    const book_id = req.params.id;

    const book = await Book.findOne(
        {
            where: {
                id: book_id
            }
        })
        if(!book){
            res.status(404).send({
                status: "Book Show ID Not Found",
            });        
        }else{
            res.status(200).send({
                status: "Book Show Data",
                book: book
            });
        }
    
});

exports.Update = asyncMiddleware(async (req, res) => {
    const book_id = req.params.id;
    const { title, author, pages, language } = req.body;
    const book = await Book.update({
        title: req.body.title,
        author: req.body.author,
        pages: req.body.pages,
        language: req.body.language
        },
        {
            where: {
                id: book_id
            }
        }
    );
    res.status(201).send({
        status: "Book update Data",

    });
});

exports.Delete = asyncMiddleware(async (req, res) => {
    const book_id = req.params.id;

    const book = await Book.destroy(
        {
            where: {
                id: book_id
            }
        }
    )
    if(!book){
        res.status(404).send({
            status: "Book Delete Not Found",
        });        
    }else{
        res.status(200).send({
            status: "Book Delete SUCCESS ",
            book: book
        });
    }
});