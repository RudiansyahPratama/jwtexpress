const db = require("../config/db.js");
const Order = db.order;
const asyncMiddleware = require("express-async-handler");

exports.create = asyncMiddleware(async (req, res) => {
    const userId = req.body.userId;
    if(userId){
    const order = await Order.create({
            userId: req.body.userId,
            bookId: req.body.bookId
        });

        res.status(200).send({
            status: "order successfully!"
        });
    }else{
        res.status(500).send({
            status: "users Failed!"
        });
    }
    });

exports.Orders = asyncMiddleware(async (req, res) => {
    const order = await Order.findAll({
        attributes: ["id", "userId", "bookId"]
    });
    
    res.status(201).send({
        status: "Order Show Data",
        data: order
    });
});

exports.OrdersID = asyncMiddleware(async (req, res) => {
    const user_id = req.params.id;
    const order = await Order.findAll(
        {
            where: {
                userId: user_id
            }
        }
    )
    if(!order){
        res.status(404).send({
            status: "Show userID Not Found",
        });        
    }else{
        res.status(201).send({
            status: "Show userID successfully!",
            data: order
        });
    }
});